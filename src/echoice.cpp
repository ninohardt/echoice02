#include "RcppArmadillo.h"
using namespace arma;
using namespace Rcpp;

// constants

const double log2pi = std::log(2.0 * M_PI);



// helper functions      ***********************************************************************************************

// these vectorized functions help speed up code when the main loop is run within R

//' Multivariate Normal Densities
//'
//' This function returns a vector of Multivariate Normal (log)-densities
//'
//' @usage dmvnrmam(x, mean, sigma, logd=FALSE)
//'
//' @param x matrix of observations
//' @param mean matrix of means matching the dimensions of x
//' @param sigma variance-covariance matrix
//' @param logd logical, if TRUE, probabilities are given as log()
//'
//' @return A vector of (log)-densities
//'
//' @examples
//' dmvnrmam(matrix(c(.5,1,1,1),2,2,byrow=T), matrix(c(0.5,0.5),2,2), diag(2))
//'
//' @export
// [[Rcpp::export]]
arma::vec dmvnrmam(arma::mat x,
                   arma::mat mean,
                   arma::mat sigma,
                   bool logd = false) {
  int n = x.n_rows;
  int xdim = x.n_cols;
  arma::vec out(n);
  arma::mat rooti = arma::trans(arma::inv(trimatu(arma::chol(sigma))));
  double rootisum = arma::sum(log(rooti.diag()));
  double constants = -(static_cast<double>(xdim)/2.0) * log2pi;

  for (int i=0; i < n; i++) {
    arma::vec z = rooti * arma::trans( x.row(i) - mean.row(i)) ;
    out(i)      = constants - 0.5 * arma::sum(z%z) + rootisum;
  }

  if (logd == false) {
    out = exp(out);
  }
  return(out);
}

//' Multivariate Normal - Generate One Random Draw
//'
//' Generates one single draw of the MVN
//' Relies on Armadillo, fast, no input checks
//'
//' @usage mvrnormArmavec(mu, sig)
//'
//' @param mu mean vector
//' @param sig Covariance Matrix
//' @return Vector; multivariate Normal draw
//'
//' @examples
//' mvrnormArmavec(1:3,diag(3)+.5)
//'
//' @export
// [[Rcpp::export]]
arma::vec mvrnormArmavec(arma::vec mu, arma::mat sigma) {
  int ncols = sigma.n_cols;
  arma::vec Y = arma::randn(ncols);
  return trans(mu.t() + Y.t() * arma::chol(sigma));
}


//' Multivariate Normal - Generate  Random Draws
//'
//' Generates MVN draws 
//' Relies on Armadillo, fast, no input checks
//'
//' @usage mvrnormArmavec(n, mu, sig)
//'
//' @param n number of draws
//' @param mu mean vector
//' @param sig Covariance Matrix
//' @return Vector; multivariate Normal draw
//'
//' @examples
//' mvrnorm(10, 1:3,diag(3)+.5)
//'
//' @export
// [[Rcpp::export]]
arma::mat mvrnorm(int n, arma::vec mu, arma::mat sigma) {
  int ncols = sigma.n_cols;
  arma::mat Y = arma::randn(n, ncols);
  return arma::repmat(mu, 1, n).t() + Y * arma::chol(sigma);
}





//' EVI random variates
//'
//'
//' @usage revd(n=100, loc=0, scale=1)
//'
//' @param n number of obs
//' @param loc location parameter
//' @param scale scale
//'
//' @return random variates
//'
//' @examples
//' revd(n=100, loc=0, scale=1)
//' @export
// [[Rcpp::export]]
vec revd(int n=100, double loc=0, double scale=1){
  return(loc-scale*log(-log(runif(n))));
}

// vec revd(int n=100, double loc=0, double scale=1){
//   return(loc - scale*log(randg(n, distr_param(1,1))));
// }




//internal function for obtaining indices of order
uvec order_a(arma::vec x) {
  return(arma::stable_sort_index( x ) );
}


//' VDM-internal: matrix row indexing for stacked design matrices
//'
//' This function picks design matrix elements related to a specified choice task
//'
//' @usage matindexer1(sel, alts, sets)
//'
//' @param sel integer, choice task to be selected, zero-based (i.e. 0 picks first task)
//' @param alts integer, number of alternatives in choice tasks
//' @param sets integer, number of choice sets that are stacked
//'
//' @return Vector of length alts*sets, where 1 denotes elements to be picked
//'
//' @examples
//' #stacked design matrix
//' stackeddesignmatrix = rbind(matrix(1:6,2,3,byrow=T), matrix(1:6,2,3,byrow=T)*10)
//'
//' #target elements: the second task (sel =1 is zero-based), two alternatives per task, 2 choice sets total
//' stackeddesignmatrix[matindexer1(1,2,2)==1]
//'
//' #easy to reconstruct design matrix for second task
//' matrix(stackeddesignmatrix[matindexer1(1,2,2)==1],2,3)
//'
//' @export
// [[Rcpp::export]]
arma::uvec matindexer1(int sel, int alts, int sets){
  int dim = alts*sets;
  arma::uvec outvec = arma::zeros<uvec>(dim);
  for (int t=0 ; t < alts; t++) {
    outvec(sel*alts+t)=1;
  }
  return(outvec);
}


// Likelihoods      ***********************************************************************************************


//' Volumetric Demand Model individual-level log-likelihood
//'
//' This function returns the log-likehood for one individual, given her design matrix, prices and observed demand quantities
//' This implementation assumes that \eqn{\sigma} is estimated.
//' EV I errors are assumed
//' 
//' Vector of parameters theta is organized as follows: \eqn{\theta  = \left\{ {\beta ,\gamma ,E,\sigma } \right\}} or c(beta,gamma,E,sigma)
//'
//' @usage vdmevs_ll(voldatai, theta)
//'
//' @param voldatai List containing matrices named \code{A}, \code{X} and \code{P}; i.e. one unit's element of a \code{voldata} list of lists
//' @param theta vector containing parameters
//'
//' @return A numeric containing the log-likelihood
//'
//' @examples
//' vdmevs_ll(voldata_demo$voldata[[1]], rnorm(20))
//' @export
// [[Rcpp::export]]
double vdmevs_ll(Rcpp::List voldatai, arma::vec theta){
  double p = theta.size();
  arma::vec beta = theta(arma::span(0,p-4));
  double bud = exp(theta(p-2));
  double gamma = exp(theta(p-3));
  double sigma = exp(theta(p-1));

  //fastest way to access list of lists elements and avoiding too many copies
  NumericMatrix dmatin = voldatai["A"];
  NumericMatrix xin = voldatai["X"];
  NumericMatrix prcsin = voldatai["P"];
  arma::mat dmat(dmatin.begin(), dmatin.rows(), dmatin.cols(), false, true);
  arma::mat x(xin.begin(), xin.rows(), xin.cols(), false, true);
  arma::mat prcs(prcsin.begin(), prcsin.rows(), prcsin.cols(), false, true);
  arma::vec xav=vectorise(x);

  const int nobs = x.n_rows;
  const int nalt = x.n_cols;

  //matrix with outside good amt for each task in each row
  const arma::mat osg= (bud - sum(x%prcs,1)) * rowvec(nalt,fill::ones);

  //aBeta matrix need reshaping to match ntask x nalt pattern
  const arma::mat aBeta = trans(reshape(dmat * beta,nalt,nobs));

  //gt for ll computation
  const arma::mat gt = -aBeta + log(gamma*x + 1) - log(osg) +log(prcs);

  //Jacobian
  const double Jacob = arma::as_scalar(
    sum(
      sum(  (log(gamma) - log(gamma*x+1)) %(x>0)  ,1)
    +   log( sum(  (((gamma*x+1)%prcs / (gamma*osg))%(x > 0))     % (x > 0)  ,1)  + 1 )
    ));

  const double out1 =  -sum(exp(-vectorise(gt)/sigma));
  const double out2 =   sum(-gt.elem( find(xav>0) )/sigma-log(sigma));

  return(Jacob+out1+out2);
}





//' Volumetric Demand Model (vectorized for multiple respondents)
//'
//' This function returns the log-likehoods for several individuals, given their design matrix, prices and observed demand quantities
//' This implementation is for when you want to estimate \eqn{\sigma}
//' It is applied to list of lists of individual-level data, avoiding loops in R
//' EV I errors are assumed
//'
//' @usage vdmevs_ll_N(datalist, thetaM)
//'
//' @param voldata List of lists, where each list contains one unit's matrices \code{A}, \code{X} and \code{P} (attributes, demand, prices)
//' @param thetaM matrix with n rows containing parameters for n individuals
//'
//' @return A vector containing the log-likelihoods 
//'
//' @examples
//' vdmevs_ll_N(dldemo, cbind(matrix(rnorm(200*18),200,18),log(sapply(dldemo,with,budget)+.5),0))
//'
//' @export
// [[Rcpp::export]]
vec vdmevs_ll_N(Rcpp::List voldata, arma::mat thetaM) {
  int nsubj = voldata.size();
  arma::vec out(nsubj);
  for(int ij=0; ij<nsubj; ij++){
    arma::vec theta=trans(thetaM.row(ij));
    Rcpp::List data=Rcpp::as<Rcpp::List>(voldata[ij]);
    out(ij)=vdmevs_ll(data, theta);
  }
  return(out);
}



// demand      ***********************************************************************************************



//' Optimal demand for Volumetric Demand Model (1 task/incidence)
//'
//' This function returns a vector of optimal demands given utilities \eqn{\psi}, satiation rate \eqn{\gamma}, budget constraint \eqn{E} and prices.
//' It is \eqn{\rm{D}} in the chapter.
//'
//' @usage vdm_demand(psi, gamma, E, prices)
//'
//' @param psi vector of utilities of all inside options in a choice set \eqn{\psi_j=a_j\beta+\varepsilon_j}
//' @param gamma numeric rate of satiation of onside good
//' @param E numeric budget constraint
//' @param prices vector of prices
//'
//' @return A vector of optimal demands
//'
//' @examples
//' vdm_demand(c(1,2),.9,10,c(2,3))
//'
//' @export
// [[Rcpp::export]]
vec vdm_demand(arma::vec psi, double gamma, double E, vec prices) {
  arma::vec rho1 = prices / (psi);
  arma::uvec rho_ord=order_a(rho1);
  rho1 = rho1.elem(rho_ord);
  int J= prices.size();

  int rhos = rho1.size();
  arma::vec rho(rhos+2);
  rho(0) =0;
  rho(rhos+1)=R_PosInf;
  for (int i = 1; i <rhos+1; ++i){
    rho(i) = rho1(i-1);
  }

  arma::vec psi_ord =psi.elem(rho_ord);
  arma::vec prices_ord = prices.elem(rho_ord);
  double a = gamma*E;
  double b = gamma;
  double k = 0;
  double z = a/b;

  while( (z<=rho[k]) | (z>rho[k+1]) ){
    a=a+arma::as_scalar(prices_ord[k]);
    b=b+arma::as_scalar(psi_ord[k]);
    z=a/b;
    k=k+1;
  }
  arma::vec x = (psi_ord*z-prices_ord) /  (gamma*prices_ord);
  for (int i = k; i <J; ++i){
    x(i) = 0;
  }
  arma::vec X =x.elem(order_a(arma::conv_to<arma::vec>::from(rho_ord)));
  return(X);
}




//' Demand based on VDM (no input checks)
//'
//' This function returns a list of lists of arrays with draws of optimal demands,
//' given draws from \eqn{theta}, and a list of lists that includes the design matrix and prices
//' It integrates over \code{epsilon}
//' EV I errors assumed
//'
//'
//' @usage vdmevs_demand_rcpp(thetaDraw, voldata)
//'
//' @param thetaDraw cube containing individual level draws from the posterior of \eqn{theta}
//' @param voldata list of lists containing P, A (Prices, Attributes)
//'
//' @return A list of lists
//'
//' @examples
//' vdmevs_demand(thetaDraw, voldata)
//'
//' @export
//[[Rcpp::export]]
List vdmevs_demand_rcpp( cube thetaDraw, List voldata ) {
  int ndraws = thetaDraw.n_rows;
  int nunits = thetaDraw.n_cols;
  int nparas = thetaDraw.n_slices; 

  List outlist(nunits);
  for (int n = 0; n <nunits; ++n){
    Rcpp::List datai=Rcpp::as<Rcpp::List>(voldata[n]);
    arma::mat prcs    = Rcpp::as<arma::mat>(datai["P"]);
    arma::mat design  = Rcpp::as<arma::mat>(datai["A"]);
    int nalt=prcs.n_cols;
    int T = prcs.n_rows;

    cube outcube(T,nalt,ndraws);
    for (int t = 0; t <T; ++t){
      arma::mat designt  = design.rows(find(matindexer1(t,nalt,T)==1));
      arma::vec prcst    = trans(prcs.row(t));
      for (int r = 0; r <ndraws; ++r){
        vec theta = thetaDraw(span(r,r),span(n,n),span(0,nparas-1));

        arma::vec beta = theta(arma::span(0,nparas-4));
        double sig = exp(theta(nparas-1));
        double gam = exp(theta(nparas-3));
        double Ex  = exp(theta(nparas-2));

        arma::vec aBetav = designt * beta;
        vec psi = exp( aBetav + revd(nalt, 0, sig));
        outcube(span(t,t),span(0,nalt-1),span(r,r)) = vdm_demand(psi, gam, Ex, prcst);
      }
    }
    outlist[n]=outcube;
  }

  return(outlist);
}





// Indirect Utility and WTP --------------------------------------------------------------


//' Indirect Utility Function of Volumetric Demand Model
//'
//' @usage vdmevsIU(theta, epsi, A, P)
//'
//' @param theta vector of parameters (part-worths, gamma, E, sigma)
//' @param epsi vector of errors of alternatives
//' @param A matrix containing attributes
//' @param P vector of prices
//'
//' @return double (Indirect Utility)
//'
//'
//' @examples
//' vdmevsIU(theta, epsi, A, P)
//'
//' @export
// [[Rcpp::export]]
double vdmevsIU(vec theta, vec epsi, mat A, vec P){
  double p    = theta.size();
  arma::vec beta = theta(arma::span(0,p-4));
  double Ex     = exp(theta(p-2));
  double gam    = exp(theta(p-3));

  int nalt   = P.size();
  vec outv(nalt);
  arma::vec aBeta = A * beta;
  arma::vec psi   = exp(aBeta + epsi  );
  outv            = vdm_demand( psi, gam, Ex,  P );
  double Z        = Ex - sum(outv%P);
  double utilsi   = sum( (psi / gam) % log(gam*outv+1)  ) + log(Z);
  return(utilsi);
}



//' @export
// [[Rcpp::export]]
double vdmevsIUe(vec theta, mat epsiM, mat design, vec prI, double Ed){
  int p = theta.size();
  theta(p-2)=log(exp(theta(p-2))+Ed);
  return(vdmevsIU(theta, epsiM, design,  prI));
}






