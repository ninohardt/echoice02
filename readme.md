# README #

### echoice ###

* Demand models with econ foundation
* Implements models and tools for modeling volumetric demand
* Applies to volumetric conjoint analysis and household-level demand data
* Current version: 0.26

### How do I get set up? ###

* Not (yet) on CRAN, but you can install via devtools

```r
library(devtools)
install_bitbucket("ninohardt/echoice", ref = "master")
```



